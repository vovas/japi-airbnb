package com.example.japiairbnb.wishlists;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.wishlists.json.ResponseJsonWishlist;
import com.example.japiairbnb.wishlists.json.ResponseJsonWishlists;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WishlistsAirbnbJsonDeserializer extends AirbnbJsonDeserializer {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public ResponseJsonWishlists createAndFillResponse(String content) {
        ResponseJsonWishlists responseJson = new ResponseJsonWishlists();
        List<ResponseJsonWishlist> wishlistsList = new ArrayList<>();

        ObjectMapper m = new ObjectMapper();
        try {
            JsonNode rootNode = m.readTree(content);

            JsonNode wishlistsNode = rootNode.path("wishlists");
            if (wishlistsNode.isArray()) {
                for (JsonNode wishlistNodeItem : wishlistsNode) {
                    ResponseJsonWishlist wishlistJson = new ResponseJsonWishlist();

                    wishlistJson.setId(wishlistNodeItem.path("id").asInt());
                    wishlistJson.setImageUrl(wishlistNodeItem.path("image_url").asText());
                    wishlistJson.setListingsCount(wishlistNodeItem.path("listings_count").asInt());
                    wishlistJson.setName(wishlistNodeItem.path("name").asText());
                    wishlistJson.setXlImageUrl(wishlistNodeItem.path("xl_image_url").asText());

                    wishlistsList.add(wishlistJson);
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization failed!", e);
        }
        responseJson.setWishlists(wishlistsList);
        return responseJson;
    }
}
