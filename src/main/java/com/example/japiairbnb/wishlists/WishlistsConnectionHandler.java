package com.example.japiairbnb.wishlists;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;

public class WishlistsConnectionHandler extends ConnectionHandler {

    @Override
    protected String getUrlName(CommonJsonRequest listingRequest) {
        return "https://api.airbnb.com/api/v2/wishlists?category=popular&_format=for_web_index_v2_logged_out";
    }
}
