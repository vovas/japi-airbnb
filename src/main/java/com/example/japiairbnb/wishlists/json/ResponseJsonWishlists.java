package com.example.japiairbnb.wishlists.json;


import com.example.japiairbnb.common.json.CommonJsonResponse;

import java.util.List;

public class ResponseJsonWishlists extends CommonJsonResponse {

    private List<ResponseJsonWishlist> wishlists;

    public List<ResponseJsonWishlist> getWishlists() {
        return wishlists;
    }

    public void setWishlists(List<ResponseJsonWishlist> wishlists) {
        this.wishlists = wishlists;
    }
}
