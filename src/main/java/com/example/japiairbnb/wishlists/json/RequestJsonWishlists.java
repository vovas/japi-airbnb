package com.example.japiairbnb.wishlists.json;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.wishlists.WishlistsAirbnbJsonDeserializer;
import com.example.japiairbnb.wishlists.WishlistsConnectionHandler;
import com.example.japiairbnb.wishlists.WishlistsRequestValidator;


public class RequestJsonWishlists extends CommonJsonRequest {



    @Override
    public ConnectionHandler getConnectionHandler() {
        return new WishlistsConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new WishlistsRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new WishlistsAirbnbJsonDeserializer();
    }
}
