package com.example.japiairbnb.wishlists.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonWishlist {

    private int id;
    @JsonProperty(value = "image_url")
    private String imageUrl;
    @JsonProperty(value = "listings_count")
    private int listingsCount;
    private String name;
    @JsonProperty(value = "xl_image_url")
    private String xlImageUrl;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getListingsCount() {
        return listingsCount;
    }

    public void setListingsCount(int listingsCount) {
        this.listingsCount = listingsCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXlImageUrl() {
        return xlImageUrl;
    }

    public void setXlImageUrl(String xlImageUrl) {
        this.xlImageUrl = xlImageUrl;
    }
}
