package com.example.japiairbnb.listingreviews;

import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.listingreviews.json.RequestJsonListingReviews;
import com.example.japiairbnb.common.RequestValidator;

public class ListingReviewsRequestValidator extends RequestValidator {

    @Override
    public boolean isRequestValid(CommonJsonRequest listingRequest) {
        RequestJsonListingReviews request = (RequestJsonListingReviews) listingRequest;
        return !(listingRequest.getAirbnbOauthToken() == null) && request.getListingId() != 0;
    }

    @Override
    public String getMessage() {
        return "Airbnb Oauth token can not be null and/or listing_id cannot be 0";
    }
}
