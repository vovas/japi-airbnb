package com.example.japiairbnb.listingreviews;

import com.example.japiairbnb.common.RequestHandler;
import com.example.japiairbnb.listingreviews.json.RequestJsonListingReviews;
import com.example.japiairbnb.common.json.CommonJsonResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ListingReviewsRequestController {

    @PostMapping("/listingreviews")
    public ResponseEntity<CommonJsonResponse> listingPostRequest(@RequestBody RequestJsonListingReviews requestJson) {

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

    @GetMapping("/listingreviews")
    public ResponseEntity<CommonJsonResponse> listingGetRequest(
            @RequestParam(value = "airbnb_advertising_id", required = false) String airbnb_advertising_id,
            @RequestParam(value = "airbnb_api_key", required = false) String airbnb_api_key,
            @RequestParam(value = "airbnb_device_id", required = false) String airbnb_device_id,
            @RequestParam(value = "airbnb_oauth_token", required = false) String airbnb_oauth_token,
            @RequestParam(value = "listing_id", required = false) Integer listingId) {

        RequestJsonListingReviews requestJson = new RequestJsonListingReviews();
        requestJson.setAirbnbAdvertisingId(airbnb_advertising_id != null ? airbnb_advertising_id : "");
        requestJson.setAirbnbApiKey(airbnb_api_key != null ? airbnb_api_key : "");
        requestJson.setAirbnbDeviceId(airbnb_device_id != null ? airbnb_device_id : "");
        requestJson.setAirbnbOauthToken(airbnb_oauth_token != null ? airbnb_oauth_token : "");
        requestJson.setListingId(listingId != null ? listingId : 0);

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

}
