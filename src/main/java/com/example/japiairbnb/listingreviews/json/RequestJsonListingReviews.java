package com.example.japiairbnb.listingreviews.json;

import com.example.japiairbnb.listingreviews.ListingReviewsAirbnbJsonDeserializer;
import com.example.japiairbnb.listingreviews.ListingReviewsConnectionHandler;
import com.example.japiairbnb.listingreviews.ListingReviewsRequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.common.*;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestJsonListingReviews extends CommonJsonRequest {

    @JsonProperty(value = "listing_id")
    private int listingId;


    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }



    @Override
    public ConnectionHandler getConnectionHandler() {
        return new ListingReviewsConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new ListingReviewsRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new ListingReviewsAirbnbJsonDeserializer();
    }
}
