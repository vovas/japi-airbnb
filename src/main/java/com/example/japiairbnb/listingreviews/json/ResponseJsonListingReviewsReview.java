package com.example.japiairbnb.listingreviews.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonListingReviewsReview {

    private String comments;
    @JsonProperty(value = "created_at")
    private String createdAt;
    private int id;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
