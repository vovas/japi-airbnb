package com.example.japiairbnb.listingreviews.json;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponseJsonListingReviews extends CommonJsonResponse {

    @JsonProperty(value = "Reviews")
    private List<ResponseJsonListingReviewsReview> reviews;

    public List<ResponseJsonListingReviewsReview> getReviews() {
        return reviews;
    }

    public void setReviews(List<ResponseJsonListingReviewsReview> reviews) {
        this.reviews = reviews;
    }
}
