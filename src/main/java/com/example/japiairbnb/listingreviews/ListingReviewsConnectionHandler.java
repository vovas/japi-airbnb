package com.example.japiairbnb.listingreviews;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.listingreviews.json.RequestJsonListingReviews;

public class ListingReviewsConnectionHandler extends ConnectionHandler {

    private static final String URL_PART1 = "https://api.airbnb.com/v2/reviews";


    @Override
    protected String getUrlName(CommonJsonRequest listingRequest) {
        RequestJsonListingReviews request = (RequestJsonListingReviews) listingRequest;

        String listingId = Integer.toString(request.getListingId());

        return URL_PART1 + "?listing_id=" + listingId + "&role=all";
    }
}
