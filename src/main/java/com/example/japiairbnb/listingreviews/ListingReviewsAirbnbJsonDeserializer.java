package com.example.japiairbnb.listingreviews;

import com.example.japiairbnb.listingreviews.json.ResponseJsonListingReviews;
import com.example.japiairbnb.listingreviews.json.ResponseJsonListingReviewsReview;
import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListingReviewsAirbnbJsonDeserializer extends AirbnbJsonDeserializer {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public ResponseJsonListingReviews createAndFillResponse(String content) {
        ResponseJsonListingReviews responseJson = new ResponseJsonListingReviews();
        List<ResponseJsonListingReviewsReview> reviewList = new ArrayList<>();

        ObjectMapper m = new ObjectMapper();
        try {
            JsonNode rootNode = m.readTree(content);

            JsonNode reviewsNode = rootNode.path("reviews");
            if (reviewsNode.isArray()) {
                for (JsonNode reviewsNodeItem : reviewsNode) {
                    ResponseJsonListingReviewsReview reviewJson = new ResponseJsonListingReviewsReview();
                    reviewJson.setComments(reviewsNodeItem.path("comments").asText());
                    //String date = reviewsNodeItem.path("created_at").asText();
                    //Instant inst = Instant.parse(date);
                    //LocalDateTime localDateTime = LocalDateTime.ofInstant(inst, ZoneOffset.UTC);
                    //LocalDateTime localDateTime = LocalDateTime.parse(date, DateTimeFormatter.ISO_INSTANT);
                    reviewJson.setCreatedAt(reviewsNodeItem.path("created_at").asText());
                    reviewJson.setId(reviewsNodeItem.path("id").asInt());

                    reviewList.add(reviewJson);
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization failed!", e);
        }
        responseJson.setReviews(reviewList);
        return responseJson;
    }
}
