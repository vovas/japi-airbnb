package com.example.japiairbnb.searchlistings.json;

import com.example.japiairbnb.searchlistings.SearchListingsAirbnbJsonDeserializer;
import com.example.japiairbnb.searchlistings.SearchListingsConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.searchlistings.SearchListingsRequestValidator;
import com.example.japiairbnb.common.*;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestJsonSearchListings extends CommonJsonRequest {

    private String location;
    @JsonProperty(value = "pets_allowed")
    private boolean petsAllowed;
    @JsonProperty(value = "min_pic_count")
    private int minPicCount;
    @JsonProperty(value = "num_guests")
    private int numGuests;
    @JsonProperty(value = "num_adults")
    private int numAdults;
    @JsonProperty(value = "_offset")
    private int offset;


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isPetsAllowed() {
        return petsAllowed;
    }

    public void setPetsAllowed(boolean petsAllowed) {
        this.petsAllowed = petsAllowed;
    }

    public int getMinPicCount() {
        return minPicCount;
    }

    public void setMinPicCount(int minPicCount) {
        this.minPicCount = minPicCount;
    }

    public int getNumGuests() {
        return numGuests;
    }

    public void setNumGuests(int numGuests) {
        this.numGuests = numGuests;
    }

    public int getNumAdults() {
        return numAdults;
    }

    public void setNumAdults(int numAdults) {
        this.numAdults = numAdults;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }


    @Override
    public ConnectionHandler getConnectionHandler() {
        return new SearchListingsConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new SearchListingsRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new SearchListingsAirbnbJsonDeserializer();
    }
}
