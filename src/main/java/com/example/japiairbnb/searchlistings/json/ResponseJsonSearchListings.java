package com.example.japiairbnb.searchlistings.json;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponseJsonSearchListings extends CommonJsonResponse {
    @JsonProperty(value = "Listings")
    private List<ResponseJsonSearchListingsListing> listings;
    @JsonProperty(value = "Pagination")
    private ResponseJsonSearchListingsPagination pagination;


    public List<ResponseJsonSearchListingsListing> getListings() {
        return listings;
    }

    public void setListings(List<ResponseJsonSearchListingsListing> listings) {
        this.listings = listings;
    }

    public ResponseJsonSearchListingsPagination getPagination() {
        return pagination;
    }

    public void setPagination(ResponseJsonSearchListingsPagination pagination) {
        this.pagination = pagination;
    }

}
