package com.example.japiairbnb.searchlistings;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.searchlistings.json.RequestJsonSearchListings;

public class SearchListingsConnectionHandler extends ConnectionHandler {

    private static final String URL_PART1 = "https://api.airbnb.com/v2/search_results";


    @Override
    protected String getUrlName(CommonJsonRequest listingRequestInit) {
        RequestJsonSearchListings listingRequest = (RequestJsonSearchListings) listingRequestInit;

        String locationName = (listingRequest.getLocation() != null) ? listingRequest.getLocation() : "";
        String petsAllowdName = listingRequest.isPetsAllowed() ? "true" : "false";
        String numGuestsName = listingRequest.getNumGuests() == 0 ? Integer.toString(1) : Integer.toString(listingRequest.getNumGuests());
        String numAdultsName = listingRequest.getNumAdults() == 0 ? Integer.toString(1) : Integer.toString(listingRequest.getNumAdults());
        String minPicCountName = Integer.toString(listingRequest.getMinPicCount());
        String minSectionOffsetName = Integer.toString(listingRequest.getOffset());

        return URL_PART1 + "?location=" + locationName + "&pets_allowed=" + petsAllowdName
                + "&num_guests=" + numGuestsName + "&num_adults=" + numAdultsName
                + "&min_pic_count=" + minPicCountName + "&_offset=" + minSectionOffsetName + "&_limit=20";
    }
}
