package com.example.japiairbnb.searchlistings;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.example.japiairbnb.common.RequestHandler;
import com.example.japiairbnb.searchlistings.json.RequestJsonSearchListings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class SearchListingsRequestController {

    @PostMapping("/searchlistings")
    public ResponseEntity<CommonJsonResponse> listingPostRequest(@RequestBody RequestJsonSearchListings requestJson) {

        CommonJsonResponse listResponse = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(listResponse, HttpStatus.valueOf(listResponse.getResponseCode()));
    }

    @GetMapping("/searchlistings")
    public ResponseEntity<CommonJsonResponse> listingGetRequest(
            @RequestParam(value = "airbnb_advertising_id", required = false) String airbnb_advertising_id,
            @RequestParam(value = "airbnb_api_key", required = false) String airbnb_api_key,
            @RequestParam(value = "airbnb_device_id", required = false) String airbnb_device_id,
            @RequestParam(value = "airbnb_oauth_token") String airbnb_oauth_token,
            @RequestParam(value = "location", required = false) String location,
            @RequestParam(value = "pets_allowed", required = false) Boolean pets_allowed,
            @RequestParam(value = "num_guests", required = false) Integer num_guests,
            @RequestParam(value = "num_adults", required = false) Integer num_adults,
            @RequestParam(value = "min_pic_count", required = false) Integer min_pic_count,
            @RequestParam(value = "_offset", required = false) Integer _offset) {

        RequestJsonSearchListings requestJson = new RequestJsonSearchListings();
        requestJson.setAirbnbAdvertisingId(airbnb_advertising_id != null ? airbnb_advertising_id : "");
        requestJson.setAirbnbApiKey(airbnb_api_key != null ? airbnb_api_key : "");
        requestJson.setAirbnbDeviceId(airbnb_device_id != null ? airbnb_device_id : "");
        requestJson.setAirbnbOauthToken(airbnb_oauth_token != null ? airbnb_oauth_token : "");
        requestJson.setLocation(location != null ? location : "");
        requestJson.setPetsAllowed(pets_allowed != null ? pets_allowed : false);
        requestJson.setNumGuests(num_guests != null ? num_guests : 1);
        requestJson.setNumAdults(num_adults != null ? num_adults : 1);
        requestJson.setMinPicCount(min_pic_count != null ? min_pic_count : 0);
        requestJson.setOffset(_offset != null ? _offset : 0);

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

}
