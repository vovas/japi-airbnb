package com.example.japiairbnb.similarlistings.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonSimilarListingsListing {

    private int id;
    private String name;
    private String neighborhood;
    @JsonProperty(value = "property_type")
    private String propertyType;
    @JsonProperty(value = "reviews_count")
    private int reviewsCount;
    @JsonProperty(value = "star_rating")
    private double starRating;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public int getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(int reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public double getStarRating() {
        return starRating;
    }

    public void setStarRating(double starRating) {
        this.starRating = starRating;
    }
}
