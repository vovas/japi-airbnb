package com.example.japiairbnb.similarlistings.json;


public class ResponseJsonSimilarListingsListingHolder {

    private ResponseJsonSimilarListingsListing listing;


    public ResponseJsonSimilarListingsListing getListing() {
        return listing;
    }

    public void setListing(ResponseJsonSimilarListingsListing listing) {
        this.listing = listing;
    }
}
