package com.example.japiairbnb.similarlistings.json;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.similarlistings.SimilarListingsAirbnbJsonDeserializer;
import com.example.japiairbnb.similarlistings.SimilarListingsConnectionHandler;
import com.example.japiairbnb.similarlistings.SimilarListingsRequestValidator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class RequestJsonSimilarListings extends CommonJsonRequest {

    @JsonProperty(value = "listing_id")
    private int listingId;
    private String checkin;
    private String checkout;

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    @Override
    public ConnectionHandler getConnectionHandler() {
        return new SimilarListingsConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new SimilarListingsRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new SimilarListingsAirbnbJsonDeserializer();
    }
}
