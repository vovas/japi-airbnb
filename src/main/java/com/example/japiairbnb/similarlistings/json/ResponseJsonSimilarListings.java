package com.example.japiairbnb.similarlistings.json;


import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponseJsonSimilarListings extends CommonJsonResponse {

    @JsonProperty(value = "similar_listings")
    private List<ResponseJsonSimilarListingsListingHolder> similarListings;


    public List<ResponseJsonSimilarListingsListingHolder> getSimilarListings() {
        return similarListings;
    }

    public void setSimilarListings(List<ResponseJsonSimilarListingsListingHolder> similarListings) {
        this.similarListings = similarListings;
    }
}
