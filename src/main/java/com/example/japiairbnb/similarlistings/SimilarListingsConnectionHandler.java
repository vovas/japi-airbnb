package com.example.japiairbnb.similarlistings;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.similarlistings.json.RequestJsonSimilarListings;

public class SimilarListingsConnectionHandler extends ConnectionHandler {

    private static final String URL_PART1 = "https://api.airbnb.com/v2/similar_listings";


    @Override
    protected String getUrlName(CommonJsonRequest listingRequest) {
        RequestJsonSimilarListings request = (RequestJsonSimilarListings) listingRequest;

        String listingId = Integer.toString(request.getListingId());

        String checkin = request.getCheckin().isEmpty() ? "" : "&checkin=" + request.getCheckin();
        String checkout = request.getCheckout().isEmpty() ? "" : "&checkout=" + request.getCheckout();

        return URL_PART1 + "?listing_id=" + listingId + checkin + checkout
                 + "&_format=for_listing_card";
    }
}
