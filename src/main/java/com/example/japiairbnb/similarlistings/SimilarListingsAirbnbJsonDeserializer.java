package com.example.japiairbnb.similarlistings;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.similarlistings.json.ResponseJsonSimilarListings;
import com.example.japiairbnb.similarlistings.json.ResponseJsonSimilarListingsListing;
import com.example.japiairbnb.similarlistings.json.ResponseJsonSimilarListingsListingHolder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimilarListingsAirbnbJsonDeserializer extends AirbnbJsonDeserializer {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public ResponseJsonSimilarListings createAndFillResponse(String content) {
        ResponseJsonSimilarListings responseJson = new ResponseJsonSimilarListings();
        List<ResponseJsonSimilarListingsListingHolder> listingList = new ArrayList<>();

        ObjectMapper m = new ObjectMapper();
        try {
            JsonNode rootNode = m.readTree(content);

            JsonNode similarListingNode = rootNode.path("similar_listings");
            if (similarListingNode.isArray()) {
                for (JsonNode simListingNodeItem : similarListingNode) {
                    ResponseJsonSimilarListingsListingHolder listingHolderJson = new ResponseJsonSimilarListingsListingHolder();
                    ResponseJsonSimilarListingsListing listingJson = new ResponseJsonSimilarListingsListing();
                    JsonNode listingNode = simListingNodeItem.path("listing");

                    listingJson.setId(listingNode.path("id").asInt());
                    listingJson.setName(listingNode.path("name").asText());
                    listingJson.setNeighborhood(listingNode.path("neighborhood").asText());
                    listingJson.setPropertyType(listingNode.path("property_type").asText());
                    listingJson.setReviewsCount(listingNode.path("reviews_count").asInt());
                    listingJson.setStarRating(listingNode.path("star_rating").asDouble());

                    listingHolderJson.setListing(listingJson);
                    listingList.add(listingHolderJson);
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization failed!", e);
        }
        responseJson.setSimilarListings(listingList);
        return responseJson;
    }
}
