package com.example.japiairbnb.similarlistings;

import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.similarlistings.json.RequestJsonSimilarListings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimilarListingsRequestValidator extends RequestValidator {

    @Override
    public boolean isRequestValid(CommonJsonRequest listingRequest) {
        RequestJsonSimilarListings request = (RequestJsonSimilarListings) listingRequest;
        return !(listingRequest.getAirbnbOauthToken() == null)
                && request.getListingId() != 0
                && (request.getCheckin().isEmpty() || isValidDate(request.getCheckin()))
                && (request.getCheckout().isEmpty() || isValidDate(request.getCheckout()));
    }

    @Override
    public String getMessage() {
        return "Airbnb Oauth token can not be null and/or listing_id cannot be 0 and/or date should be valid";
    }

    private boolean isValidDate(String checkin) {
        Pattern ptn = Pattern.compile("^([0-9]{4})-?(1[0-2]|0[1-9])-?(3[01]|0[1-9]|[12][0-9])$");
        Matcher matcher = ptn.matcher(checkin);
        return matcher.matches();
    }
}
