package com.example.japiairbnb.similarlistings;

import com.example.japiairbnb.common.RequestHandler;
import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.example.japiairbnb.similarlistings.json.RequestJsonSimilarListings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SimilarListingsRequestController {

    @PostMapping("/similarlistings")
    public ResponseEntity<CommonJsonResponse> listingPostRequest(@RequestBody RequestJsonSimilarListings requestJson) {

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

    @GetMapping("/similarlistings")
    public ResponseEntity<CommonJsonResponse> listingGetRequest(
            @RequestParam(value = "airbnb_advertising_id", required = false) String airbnb_advertising_id,
            @RequestParam(value = "airbnb_api_key", required = false) String airbnb_api_key,
            @RequestParam(value = "airbnb_device_id", required = false) String airbnb_device_id,
            @RequestParam(value = "airbnb_oauth_token", required = false) String airbnb_oauth_token,
            @RequestParam(value = "listing_id", required = false) Integer listingId,
            @RequestParam(value = "checkin", required = false) String checkin,
            @RequestParam(value = "checkout", required = false) String checkout) {

        RequestJsonSimilarListings requestJson = new RequestJsonSimilarListings();
        requestJson.setAirbnbAdvertisingId(airbnb_advertising_id != null ? airbnb_advertising_id : "");
        requestJson.setAirbnbApiKey(airbnb_api_key != null ? airbnb_api_key : "");
        requestJson.setAirbnbDeviceId(airbnb_device_id != null ? airbnb_device_id : "");
        requestJson.setAirbnbOauthToken(airbnb_oauth_token != null ? airbnb_oauth_token : "");
        requestJson.setListingId(listingId != null ? listingId : 0);
        requestJson.setCheckin(checkin != null ? checkin : "");
        requestJson.setCheckout(checkout != null ? checkout : "");

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

}
