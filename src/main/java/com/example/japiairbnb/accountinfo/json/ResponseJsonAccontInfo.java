package com.example.japiairbnb.accountinfo.json;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseJsonAccontInfo extends CommonJsonResponse {

    @JsonProperty(value = "acceptance_rate")
    private String acceptanceRate;

    @JsonProperty(value = "created_at")
    private String createdAt;

    @JsonProperty(value = "friends_count")
    private int friendCount;

    private int id;

    @JsonProperty(value = "listings_count")
    private int listingsCount;

    @JsonProperty(value = "recommendation_count")
    private int recommendationCount;

    @JsonProperty(value = "response_rate")
    private String responseRate;

    @JsonProperty(value = "response_time")
    private String responseTime;

    @JsonProperty(value = "reviewee_count")
    private int revieweeCount;

    @JsonProperty(value = "total_listings_count")
    private int totalListingsCount;

    @JsonProperty(value = "has_available_payout_info")
    private boolean hasAvailablePayoutInfo;

    @JsonProperty(value = "has_profile_pic")
    private boolean hasProfilePic;

    @JsonProperty(value = "identity_verified")
    private boolean identityVerified;



    public String getAcceptanceRate() {
        return acceptanceRate;
    }

    public void setAcceptanceRate(String acceptanceRate) {
        this.acceptanceRate = acceptanceRate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getFriendCount() {
        return friendCount;
    }

    public void setFriendCount(int friendCount) {
        this.friendCount = friendCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getListingsCount() {
        return listingsCount;
    }

    public void setListingsCount(int listingsCount) {
        this.listingsCount = listingsCount;
    }

    public int getRecommendationCount() {
        return recommendationCount;
    }

    public void setRecommendationCount(int recommendationCount) {
        this.recommendationCount = recommendationCount;
    }

    public String getResponseRate() {
        return responseRate;
    }

    public void setResponseRate(String responseRate) {
        this.responseRate = responseRate;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public int getRevieweeCount() {
        return revieweeCount;
    }

    public void setRevieweeCount(int revieweeCount) {
        this.revieweeCount = revieweeCount;
    }

    public int getTotalListingsCount() {
        return totalListingsCount;
    }

    public void setTotalListingsCount(int totalListingsCount) {
        this.totalListingsCount = totalListingsCount;
    }

    public boolean isHasAvailablePayoutInfo() {
        return hasAvailablePayoutInfo;
    }

    public void setHasAvailablePayoutInfo(boolean hasAvailablePayoutInfo) {
        this.hasAvailablePayoutInfo = hasAvailablePayoutInfo;
    }

    public boolean isHasProfilePic() {
        return hasProfilePic;
    }

    public void setHasProfilePic(boolean hasProfilePic) {
        this.hasProfilePic = hasProfilePic;
    }

    public boolean isIdentityVerified() {
        return identityVerified;
    }

    public void setIdentityVerified(boolean identityVerified) {
        this.identityVerified = identityVerified;
    }
}
