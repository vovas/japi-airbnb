package com.example.japiairbnb.accountinfo.json;

import com.example.japiairbnb.accountinfo.AccountInfoConnectionHandler;
import com.example.japiairbnb.accountinfo.AccountInfoJsonDeserializer;
import com.example.japiairbnb.accountinfo.AccountInfoRequestValidator;
import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;

public class RequestJsonAccountInfo extends CommonJsonRequest {


    @Override
    public ConnectionHandler getConnectionHandler() {
        return new AccountInfoConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new AccountInfoRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new AccountInfoJsonDeserializer();
    }
}
