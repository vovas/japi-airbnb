package com.example.japiairbnb.accountinfo;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;

public class AccountInfoConnectionHandler extends ConnectionHandler {

    @Override
    protected String getUrlName(CommonJsonRequest listingRequest) {
        return "https://api.airbnb.com/v2/users/me";
    }
}
