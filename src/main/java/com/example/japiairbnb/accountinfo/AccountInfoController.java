package com.example.japiairbnb.accountinfo;

import com.example.japiairbnb.accountinfo.json.RequestJsonAccountInfo;
import com.example.japiairbnb.common.RequestHandler;
import com.example.japiairbnb.common.json.CommonJsonResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class AccountInfoController {

    @PostMapping("/accountinfo")
    public ResponseEntity<CommonJsonResponse> listingPostRequest(@RequestBody RequestJsonAccountInfo requestJson) {

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

    @GetMapping("/accountinfo")
    public ResponseEntity<CommonJsonResponse> listingGetRequest(
            @RequestParam(value = "airbnb_advertising_id", required = false) String airbnb_advertising_id,
            @RequestParam(value = "airbnb_api_key", required = false) String airbnb_api_key,
            @RequestParam(value = "airbnb_device_id", required = false) String airbnb_device_id,
            @RequestParam(value = "airbnb_oauth_token") String airbnb_oauth_token) {

        RequestJsonAccountInfo requestJson = new RequestJsonAccountInfo();
        requestJson.setAirbnbAdvertisingId(airbnb_advertising_id != null ? airbnb_advertising_id : "");
        requestJson.setAirbnbApiKey(airbnb_api_key != null ? airbnb_api_key : "");
        requestJson.setAirbnbDeviceId(airbnb_device_id != null ? airbnb_device_id : "");
        requestJson.setAirbnbOauthToken(airbnb_oauth_token != null ? airbnb_oauth_token : "");

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }
}
