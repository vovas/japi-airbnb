package com.example.japiairbnb.accountinfo;

import com.example.japiairbnb.accountinfo.json.ResponseJsonAccontInfo;
import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountInfoJsonDeserializer extends AirbnbJsonDeserializer {

    private final Logger logger = Logger.getLogger(this.getClass().getName());


    @Override
    public CommonJsonResponse createAndFillResponse(String content) {

        ObjectMapper m = new ObjectMapper();
        ResponseJsonAccontInfo userJson = new ResponseJsonAccontInfo();

        try {
            JsonNode rootNode = m.readTree(content);
            JsonNode userNode = rootNode.path("user");

            userJson.setAcceptanceRate(userNode.path("acceptance_rate").asText());
            userJson.setCreatedAt(userNode.path("created_at").asText());
            userJson.setFriendCount(userNode.path("friends_count").asInt());
            userJson.setId(userNode.path("id").asInt());
            userJson.setListingsCount(userNode.path("listings_count").asInt());

            userJson.setRecommendationCount(userNode.path("recommendation_count").asInt());
            userJson.setResponseRate(userNode.path("response_rate").asText());
            userJson.setResponseTime(userNode.path("response_time").asText());
            userJson.setRevieweeCount(userNode.path("reviewee_count").asInt());
            userJson.setTotalListingsCount(userNode.path("total_listings_count").asInt());

            userJson.setHasAvailablePayoutInfo(userNode.path("has_available_payout_info").asBoolean());
            userJson.setHasProfilePic(userNode.path("has_profile_pic").asBoolean());
            userJson.setIdentityVerified(userNode.path("identity_verified").asBoolean());

        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization failed!", e);
        }
        return userJson;
    }
}
