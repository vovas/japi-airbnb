package com.example.japiairbnb.wishlistedlistings.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonWishlistedListingsListing {

    private double bathrooms;
    private int bedrooms;
    private int beds;
    private String city;
    private String distance;
    private int id;
    @JsonProperty(value = "is_business_travel_ready")
    private boolean isBusinessTravelReady;
    @JsonProperty(value = "is_new_listing")
    private boolean newListing;
    private double lat;
    private double lng;
    private String name;
    private String neighborhood;
    @JsonProperty(value = "picture_count")
    private int pictureCount;
    @JsonProperty(value = "picture-url")
    private String pictureUrl;
    @JsonProperty(value = "preview_encoded_png")
    private String previewEncodedPng;
    @JsonProperty(value = "property_type")
    private String propertyType;
    @JsonProperty(value = "reviews_count")
    private int reviewsCount;
    @JsonProperty(value = "room_type")
    private String roomType;
    @JsonProperty(value = "star_rating")
    private double starRating;


    public double getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(double bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public int getBeds() {
        return beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isBusinessTravelReady() {
        return isBusinessTravelReady;
    }

    public void setBusinessTravelReady(boolean businessTravelReady) {
        isBusinessTravelReady = businessTravelReady;
    }

    public boolean isNewListing() {
        return newListing;
    }

    public void setNewListing(boolean newListing) {
        this.newListing = newListing;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public int getPictureCount() {
        return pictureCount;
    }

    public void setPictureCount(int pictureCount) {
        this.pictureCount = pictureCount;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPreviewEncodedPng() {
        return previewEncodedPng;
    }

    public void setPreviewEncodedPng(String previewEncodedPng) {
        this.previewEncodedPng = previewEncodedPng;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public int getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(int reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public double getStarRating() {
        return starRating;
    }

    public void setStarRating(double starRating) {
        this.starRating = starRating;
    }
}
