package com.example.japiairbnb.wishlistedlistings.json;


import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponseJsonWishlistedListingsHolder extends CommonJsonResponse {

    private ResponseJsonWishlistedListingsListing listing;
    @JsonProperty(value = "pricing_quote")
    private ResponseJsonWishlistedListingsPricingQuote pricingQuote;


    public ResponseJsonWishlistedListingsListing getListing() {
        return listing;
    }

    public void setListing(ResponseJsonWishlistedListingsListing listing) {
        this.listing = listing;
    }

    public ResponseJsonWishlistedListingsPricingQuote getPricingQuote() {
        return pricingQuote;
    }

    public void setPricingQuote(ResponseJsonWishlistedListingsPricingQuote pricingQuote) {
        this.pricingQuote = pricingQuote;
    }
}
