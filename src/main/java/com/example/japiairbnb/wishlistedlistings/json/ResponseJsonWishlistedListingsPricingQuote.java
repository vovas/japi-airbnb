package com.example.japiairbnb.wishlistedlistings.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonWishlistedListingsPricingQuote {

    private boolean available;
    @JsonProperty(value = "can_instant_book")
    private boolean canInstantBook;
    @JsonProperty(value = "rate_type")
    private String rateType;


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isCanInstantBook() {
        return canInstantBook;
    }

    public void setCanInstantBook(boolean canInstantBook) {
        this.canInstantBook = canInstantBook;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }
}
