package com.example.japiairbnb.wishlistedlistings.json;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResponseJsonWishlistedListings extends CommonJsonResponse {

    @JsonProperty(value = "wishlisted_listings")
    private List<ResponseJsonWishlistedListingsHolder> wishlistedListings;

    public List<ResponseJsonWishlistedListingsHolder> getWishlistedListings() {
        return wishlistedListings;
    }

    public void setWishlistedListings(List<ResponseJsonWishlistedListingsHolder> wishlistedListings) {
        this.wishlistedListings = wishlistedListings;
    }
}
