package com.example.japiairbnb.wishlistedlistings.json;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.wishlistedlistings.WishlistedListingsAirbnbJsonDeserializer;
import com.example.japiairbnb.wishlistedlistings.WishlistedListingsConnectionHandler;
import com.example.japiairbnb.wishlistedlistings.WishlistedListingsRequestValidator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class RequestJsonWishlistedListings extends CommonJsonRequest {

    @JsonProperty(value = "wishlist_id")
    private int wishlistId;


    public int getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(int wishlistId) {
        this.wishlistId = wishlistId;
    }


    @Override
    public ConnectionHandler getConnectionHandler() {
        return new WishlistedListingsConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new WishlistedListingsRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new WishlistedListingsAirbnbJsonDeserializer();
    }
}
