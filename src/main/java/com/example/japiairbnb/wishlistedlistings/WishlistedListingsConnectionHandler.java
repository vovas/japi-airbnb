package com.example.japiairbnb.wishlistedlistings;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.wishlistedlistings.json.RequestJsonWishlistedListings;

public class WishlistedListingsConnectionHandler extends ConnectionHandler {

    private static final String URL_PART1 = "https://api.airbnb.com/api/v2/wishlisted_listings" +
            "?_format=for_web_details_v2_public";


    @Override
    protected String getUrlName(CommonJsonRequest listingRequest) {
        RequestJsonWishlistedListings request = (RequestJsonWishlistedListings) listingRequest;

        String wishlistId = Integer.toString(request.getWishlistId());

        return URL_PART1 + "&wishlist_id=" + wishlistId;
    }
}
