package com.example.japiairbnb.wishlistedlistings;

import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.wishlistedlistings.json.RequestJsonWishlistedListings;


public class WishlistedListingsRequestValidator extends RequestValidator {

    @Override
    public boolean isRequestValid(CommonJsonRequest listingRequest) {
        RequestJsonWishlistedListings request = (RequestJsonWishlistedListings) listingRequest;
        return !(listingRequest.getAirbnbOauthToken() == null)
                && request.getWishlistId() != 0;
    }

    @Override
    public String getMessage() {
        return "Airbnb Oauth token can not be null and/or wishlist_id cannot be 0";
    }
}
