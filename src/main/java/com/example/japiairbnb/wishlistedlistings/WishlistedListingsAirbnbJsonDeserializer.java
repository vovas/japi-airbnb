package com.example.japiairbnb.wishlistedlistings;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.similarlistings.json.ResponseJsonSimilarListingsListing;
import com.example.japiairbnb.similarlistings.json.ResponseJsonSimilarListingsListingHolder;
import com.example.japiairbnb.wishlistedlistings.json.ResponseJsonWishlistedListings;
import com.example.japiairbnb.wishlistedlistings.json.ResponseJsonWishlistedListingsHolder;
import com.example.japiairbnb.wishlistedlistings.json.ResponseJsonWishlistedListingsListing;
import com.example.japiairbnb.wishlistedlistings.json.ResponseJsonWishlistedListingsPricingQuote;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WishlistedListingsAirbnbJsonDeserializer extends AirbnbJsonDeserializer {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public ResponseJsonWishlistedListings createAndFillResponse(String content) {
        ResponseJsonWishlistedListings responseJson = new ResponseJsonWishlistedListings();
        List<ResponseJsonWishlistedListingsHolder> wishlistList = new ArrayList<>();

        ObjectMapper m = new ObjectMapper();
        try {
            JsonNode rootNode = m.readTree(content);

            JsonNode wishlistedListingsNode = rootNode.path("wishlisted_listings");
            if (wishlistedListingsNode.isArray()) {
                for (JsonNode wishlistedListingsNodeItem : wishlistedListingsNode) {
                    ResponseJsonWishlistedListingsHolder wishlistingHolderJson = new ResponseJsonWishlistedListingsHolder();

                    ResponseJsonWishlistedListingsListing wishlistingJson = new ResponseJsonWishlistedListingsListing();
                    ResponseJsonWishlistedListingsPricingQuote pricingQuoteJson = new ResponseJsonWishlistedListingsPricingQuote();

                    JsonNode listingNode = wishlistedListingsNodeItem.path("listing");
                    wishlistingJson.setBathrooms(listingNode.path("bathrooms").asDouble());
                    wishlistingJson.setBedrooms(listingNode.path("bedrooms").asInt());
                    wishlistingJson.setBeds(listingNode.path("beds").asInt());
                    wishlistingJson.setCity(listingNode.path("city").asText());
                    wishlistingJson.setDistance(listingNode.path("distance").asText());
                    wishlistingJson.setId(listingNode.path("id").asInt());
                    wishlistingJson.setBusinessTravelReady(listingNode.path("is_business_travel_ready").asBoolean());
                    wishlistingJson.setNewListing(listingNode.path("is_new_listing").asBoolean());
                    wishlistingJson.setLat(listingNode.path("lat").asDouble());
                    wishlistingJson.setLng(listingNode.path("lng").asDouble());
                    wishlistingJson.setName(listingNode.path("name").asText());
                    wishlistingJson.setNeighborhood(listingNode.path("neighborhood").asText());
                    wishlistingJson.setPictureCount(listingNode.path("picture_count").asInt());
                    wishlistingJson.setPictureUrl(listingNode.path("picture_url").asText());
                    wishlistingJson.setPreviewEncodedPng(listingNode.path("preview_encoded_png").asText());
                    wishlistingJson.setPropertyType(listingNode.path("property_type").asText());
                    wishlistingJson.setReviewsCount(listingNode.path("reviews_count").asInt());
                    wishlistingJson.setRoomType(listingNode.path("room_type").asText());
                    wishlistingJson.setStarRating(listingNode.path("star_rating").asDouble());

                    JsonNode pricingQuoteNode = wishlistedListingsNodeItem.path("pricing_quote");
                    pricingQuoteJson.setAvailable(pricingQuoteNode.path("available").asBoolean());
                    pricingQuoteJson.setCanInstantBook(pricingQuoteNode.path("can_instant_book").asBoolean());
                    pricingQuoteJson.setRateType(pricingQuoteNode.path("rate_type").asText());

                    wishlistingHolderJson.setListing(wishlistingJson);
                    wishlistingHolderJson.setPricingQuote(pricingQuoteJson);

                    wishlistList.add(wishlistingHolderJson);
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization failed!", e);
        }
        responseJson.setWishlistedListings(wishlistList);
        return responseJson;
    }
}
