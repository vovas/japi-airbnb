package com.example.japiairbnb.common;

import com.example.japiairbnb.common.json.*;
import com.example.japiairbnb.searchlistings.SearchListingsAirbnbJsonDeserializer;

public class RequestHandler {
    private static final RequestHandler ourInstance = new RequestHandler();

    public static RequestHandler getInstance() {
        return ourInstance;
    }

    private RequestHandler() {}


    public CommonJsonResponse getResponse(CommonJsonRequest listingRequest) {
        CommonJsonResponse result;
        // Validate request
        RequestValidator validator = listingRequest.getValidator();
        result = validator.checkRequest(listingRequest);
        if (result != null) {
            return result;
        }
        // Sends request
        ConnectionHandler connectionHandler = listingRequest.getConnectionHandler();
        AirbnbResponseWrapper response = connectionHandler.getJsonResponse(listingRequest);

        if (isError(response)) {
            return getErrorJsonResponse(response);
        }

        AirbnbJsonDeserializer deserializer = listingRequest.getJsonDeserializer();
        CommonJsonResponse responseJson = deserializer.createAndFillResponse(response.getContent());
        responseJson.setResponseCode(response.getCode());

        return responseJson;
    }


    private boolean isError(AirbnbResponseWrapper response) {
        return response.getCode() != 200;
    }

    private CommonJsonResponse getErrorJsonResponse(AirbnbResponseWrapper response) {
        ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
        errorJsonResponse.setErrorCode(response.getCode());
        errorJsonResponse.setMessage(response.getContent());
        return errorJsonResponse;
    }


    CommonJsonResponse getToken(String username, String password) {
        CommonJsonResponse tokenResponse;
        if (!isTokenRequestValid(username, password)) {
            ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
            errorJsonResponse.setErrorCode(400);
            errorJsonResponse.setMessage("Username and/or password is null!");
            tokenResponse = errorJsonResponse;
            return tokenResponse;
        }
        String tokenJsonRequestStr = "{\"username\":\"" + username  + "\","
                + "\"password\":\"" + password + "\","
                + "\"prevent_account_creation\": true}";

        tokenResponse = getResponseFromUrl(tokenJsonRequestStr);
        return tokenResponse;
    }

    /**
     * Creates and sends request to URL
     *
     * @param tokenJsonRequestStr request for token in JSON format
     * @return response from URL
     */
    private CommonJsonResponse getResponseFromUrl(String tokenJsonRequestStr) {
        ConnectionHandlerGetToken connectionHandler = new ConnectionHandlerGetToken();
        AirbnbResponseWrapper response = connectionHandler.getJsonResponse(tokenJsonRequestStr);
        if (isError(response)) {
            return getErrorJsonResponse(response);
        }

        ResponseJsonToken responseJsonToken = new ResponseJsonToken();
        responseJsonToken.setResponseCode(response.getCode());

        SearchListingsAirbnbJsonDeserializer deserializer = new SearchListingsAirbnbJsonDeserializer();
        deserializer.setFields(responseJsonToken, response.getContent());

        return responseJsonToken;
    }

    /**
     * Checks if tokenRequest contains all necessary fields. The fields should not be null and empty
     *
     * @param username username
     * @param password password
     * @return true if request is ok
     */
    private boolean isTokenRequestValid(String username, String password) {
        return (username != null && !username.isEmpty())
                && (password != null && !password.isEmpty());
    }

}
