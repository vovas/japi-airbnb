package com.example.japiairbnb.common.json;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.RequestValidator;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class CommonJsonRequest {
    @JsonProperty(value = "airbnb_advertising_id")
    private String airbnbAdvertisingId;
    @JsonProperty(value = "airbnb_api_key")
    private String airbnbApiKey;
    @JsonProperty(value = "airbnb_device_id")
    private String airbnbDeviceId;
    @JsonProperty(value = "airbnb_oauth_token")
    private String airbnbOauthToken;


    public String getAirbnbAdvertisingId() {
        return airbnbAdvertisingId;
    }

    public void setAirbnbAdvertisingId(String airbnbAdvertisingId) {
        this.airbnbAdvertisingId = airbnbAdvertisingId;
    }

    public String getAirbnbApiKey() {
        return airbnbApiKey;
    }

    public void setAirbnbApiKey(String airbnbApiKey) {
        this.airbnbApiKey = airbnbApiKey;
    }

    public String getAirbnbDeviceId() {
        return airbnbDeviceId;
    }

    public void setAirbnbDeviceId(String airbnbDeviceId) {
        this.airbnbDeviceId = airbnbDeviceId;
    }

    public String getAirbnbOauthToken() {
        return airbnbOauthToken;
    }

    public void setAirbnbOauthToken(String airbnbOauthToken) {
        this.airbnbOauthToken = airbnbOauthToken;
    }


    public abstract ConnectionHandler getConnectionHandler();

    public abstract RequestValidator getValidator();

    public abstract AirbnbJsonDeserializer getJsonDeserializer();
}
