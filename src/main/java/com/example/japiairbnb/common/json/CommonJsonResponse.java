package com.example.japiairbnb.common.json;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CommonJsonResponse {

    @JsonIgnore
    private int responseCode;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

}
