package com.example.japiairbnb.common.json;

public abstract class AirbnbResponseWrapper {

    public abstract int getCode();
    public abstract String getContent();
}
