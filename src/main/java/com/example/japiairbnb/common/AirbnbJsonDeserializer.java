package com.example.japiairbnb.common;

import com.example.japiairbnb.common.json.CommonJsonResponse;

public abstract class AirbnbJsonDeserializer {

    public abstract CommonJsonResponse createAndFillResponse(String content);

}
