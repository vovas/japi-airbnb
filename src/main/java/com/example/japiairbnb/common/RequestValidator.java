package com.example.japiairbnb.common;

import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.example.japiairbnb.common.json.ErrorJsonResponse;

public class RequestValidator {

    public CommonJsonResponse checkRequest(CommonJsonRequest listingRequest) {
        if (!isRequestValid(listingRequest)) {
            ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
            errorJsonResponse.setErrorCode(400);
            errorJsonResponse.setMessage(getMessage());
            return errorJsonResponse;
        }
        return null;
    }

    /**
     * Checks if listingRequest contains all necessary fields
     *
     * @param listingRequest request
     * @return true if request is ok
     */
    public boolean isRequestValid(CommonJsonRequest listingRequest) {
        // check if the instance contains necessary not null fields
        return !(listingRequest.getAirbnbOauthToken() == null);
    }

    public String getMessage() {
        return "Airbnb Oauth token is null!";
    }
}
