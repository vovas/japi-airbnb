package com.example.japiairbnb.common;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class RequestControllerGetToken {

    @GetMapping("/gettoken")
    public ResponseEntity<CommonJsonResponse> getApiToken(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password) {

        CommonJsonResponse token = RequestHandler.getInstance().getToken(username, password);

        return new ResponseEntity<>(token, HttpStatus.valueOf(token.getResponseCode()));
    }

}
