package com.example.japiairbnb.common;

import com.example.japiairbnb.common.json.AirbnbMainResponse;
import com.example.japiairbnb.common.json.AirbnbResponseMessage;
import com.example.japiairbnb.common.json.AirbnbResponseWrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionHandlerGetToken {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private static final String URL_TOKEN_PART1 = "https://api.airbnb.com/v1/authorize";


    public AirbnbResponseWrapper getJsonResponse(String tokenJsonRequestStr) {

        AirbnbMainResponse mainResponse = new AirbnbMainResponse(-1, "Something wrong");
        try {
            URL urlToConnect = new URL(URL_TOKEN_PART1);
            HttpURLConnection connection = (HttpURLConnection) urlToConnect.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Host", "api.airbnb.com");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Language", "en-us");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Airbnb-Currency", "USD");
            connection.setRequestProperty("X-Airbnb-Locale", "en");
            connection.setRequestProperty("X-Airbnb-API-Key", "915pw2pnf4h1aiguhph5gc5b2");
            connection.setRequestProperty("X-Airbnb-Device-ID", "911EBF1C-7C1D-46D5-A925-2F49ED064C92");
            connection.setRequestProperty("X-Airbnb-Advertising-ID", "a382581f36f1635a78f3d688bf0f99d85ec7e21f");
            connection.setRequestProperty("User-Agent", "Airbnb/15.50 iPhone/9.2 Type/Phone");
            OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
            osw.write(tokenJsonRequestStr);
            osw.flush();
            osw.close();

            int responseCode = connection.getResponseCode();

            if(responseCode != 200) {
                AirbnbResponseMessage message = new AirbnbResponseMessage(responseCode, connection.getResponseMessage());
                connection.disconnect();
                return message;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            mainResponse = new AirbnbMainResponse(responseCode, sb.toString());
            connection.disconnect();
        } catch (MalformedURLException e) {
            logger.log(Level.WARNING, "Wrong URL!", e);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Response content reading failed!", e);
        }

        return mainResponse;
    }
}
