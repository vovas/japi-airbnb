package com.example.japiairbnb.common;

import com.example.japiairbnb.common.json.AirbnbMainResponse;
import com.example.japiairbnb.common.json.AirbnbResponseMessage;
import com.example.japiairbnb.common.json.AirbnbResponseWrapper;
import com.example.japiairbnb.common.json.CommonJsonRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ConnectionHandler {

    private final Logger logger = Logger.getLogger(this.getClass().getName());



    public AirbnbResponseWrapper getJsonResponse(CommonJsonRequest listingRequest) {

        String urlName = getUrlName(listingRequest);

        AirbnbMainResponse mainResponse = new AirbnbMainResponse(-1, "Something wrong");

        try {
            URL urlToConnect = new URL(urlName);
            HttpURLConnection connection = (HttpURLConnection) urlToConnect.openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("Host", "api.airbnb.com");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Language", "en-us");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            setHeaders(connection, listingRequest);

            int responseCode = connection.getResponseCode();
            if(responseCode != 200) {
                return new AirbnbResponseMessage(responseCode, connection.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            mainResponse = new AirbnbMainResponse(responseCode, sb.toString());
        } catch (MalformedURLException e) {
            logger.log(Level.WARNING, "Wrong URL!", e);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Response content reading failed!", e);
        }
        return mainResponse;
    }


    private void setHeaders(HttpURLConnection connection, CommonJsonRequest listingRequest) {
        connection.setRequestProperty("X-Airbnb-API-Key", listingRequest.getAirbnbApiKey());
        connection.setRequestProperty("X-Airbnb-Device-ID", listingRequest.getAirbnbDeviceId());
        connection.setRequestProperty("X-Airbnb-Advertising-ID", listingRequest.getAirbnbAdvertisingId());
        connection.setRequestProperty("X-Airbnb-OAuth-Token", listingRequest.getAirbnbOauthToken());
        connection.setRequestProperty("User-Agent", "Airbnb/15.50 iPhone/9.2 Type/Phone");
    }


    protected abstract String getUrlName(CommonJsonRequest listingRequest);

}
