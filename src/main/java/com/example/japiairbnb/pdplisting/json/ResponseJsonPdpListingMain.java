package com.example.japiairbnb.pdplisting.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonPdpListingMain {

    private boolean available;
    @JsonProperty(value = "can_instant_book")
    private boolean canInstantBook;
    @JsonProperty(value = "cancellation_guest_fee_policy_label")
    private String cancellationGuestFeePolicyLabel;
    private int guests;
    private ResponseJsonPdpListingRate rate;
    @JsonProperty(value = "rate_type")
    private String rateType;
    @JsonProperty(value = "service_fee_usd")
    private double serviceFeeUsd;
    @JsonProperty(value = "tax_amount_usd")
    private double taxAmountUsd;


    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isCanInstantBook() {
        return canInstantBook;
    }

    public void setCanInstantBook(boolean canInstantBook) {
        this.canInstantBook = canInstantBook;
    }

    public String getCancellationGuestFeePolicyLabel() {
        return cancellationGuestFeePolicyLabel;
    }

    public void setCancellationGuestFeePolicyLabel(String cancellationGuestFeePolicyLabel) {
        this.cancellationGuestFeePolicyLabel = cancellationGuestFeePolicyLabel;
    }

    public int getGuests() {
        return guests;
    }

    public void setGuests(int guests) {
        this.guests = guests;
    }

    public ResponseJsonPdpListingRate getRate() {
        return rate;
    }

    public void setRate(ResponseJsonPdpListingRate rate) {
        this.rate = rate;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public double getServiceFeeUsd() {
        return serviceFeeUsd;
    }

    public void setServiceFeeUsd(double serviceFeeUsd) {
        this.serviceFeeUsd = serviceFeeUsd;
    }

    public double getTaxAmountUsd() {
        return taxAmountUsd;
    }

    public void setTaxAmountUsd(double taxAmountUsd) {
        this.taxAmountUsd = taxAmountUsd;
    }
}
