package com.example.japiairbnb.pdplisting.json;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseJsonPdpListingRate {

    private int amount;
    @JsonProperty(value = "amount_micros")
    private int amountMicros;
    @JsonProperty(value = "amount_formatted")
    private String amountFormatted;
    @JsonProperty(value = "is_micros_accuracy")
    private boolean isMicrosAccuracy;
    private String currency;


    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmountMicros() {
        return amountMicros;
    }

    public void setAmountMicros(int amountMicros) {
        this.amountMicros = amountMicros;
    }

    public String getAmountFormatted() {
        return amountFormatted;
    }

    public void setAmountFormatted(String amountFormatted) {
        this.amountFormatted = amountFormatted;
    }

    public boolean isMicrosAccuracy() {
        return isMicrosAccuracy;
    }

    public void setMicrosAccuracy(boolean microsAccuracy) {
        isMicrosAccuracy = microsAccuracy;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
