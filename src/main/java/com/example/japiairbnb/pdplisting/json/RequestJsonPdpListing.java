package com.example.japiairbnb.pdplisting.json;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.pdplisting.PdpListingAirbnbJsonDeserializer;
import com.example.japiairbnb.pdplisting.PdpListingConnectionHandler;
import com.example.japiairbnb.pdplisting.PdpListingRequestValidator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestJsonPdpListing extends CommonJsonRequest {

    @JsonProperty(value = "listing_id")
    private int listingId;
    private int guests;
    @JsonProperty(value = "show_smart_promotion")
    private int showSmartPromotion;
    @JsonProperty(value = "number_of_adults")
    private int numberOfAdults;
    @JsonProperty(value = "number_of_children")
    private int numberOfChildren;
    @JsonProperty(value = "number_of_infants")
    private int numberOfInfants;


    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public int getGuests() {
        return guests;
    }

    public void setGuests(int guests) {
        this.guests = guests;
    }

    public int getShowSmartPromotion() {
        return showSmartPromotion;
    }

    public void setShowSmartPromotion(int showSmartPromotion) {
        this.showSmartPromotion = showSmartPromotion;
    }

    public int getNumberOfAdults() {
        return numberOfAdults;
    }

    public void setNumberOfAdults(int numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public int getNumberOfInfants() {
        return numberOfInfants;
    }

    public void setNumberOfInfants(int numberOfInfants) {
        this.numberOfInfants = numberOfInfants;
    }

    @Override
    public ConnectionHandler getConnectionHandler() {
        return new PdpListingConnectionHandler();
    }

    @Override
    public RequestValidator getValidator() {
        return new PdpListingRequestValidator();
    }

    @Override
    public AirbnbJsonDeserializer getJsonDeserializer() {
        return new PdpListingAirbnbJsonDeserializer();
    }
}
