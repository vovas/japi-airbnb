package com.example.japiairbnb.pdplisting.json;

import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class ResponseJsonPdpListing extends CommonJsonResponse {

    @JsonProperty(value = "pdp_listing_booking_details")
    private List<ResponseJsonPdpListingMain> pdpListingBookingDetails;


    public List<ResponseJsonPdpListingMain> getPdpListingBookingDetails() {
        return pdpListingBookingDetails;
    }

    public void setPdpListingBookingDetails(List<ResponseJsonPdpListingMain> pdpListingBookingDetails) {
        this.pdpListingBookingDetails = pdpListingBookingDetails;
    }
}
