package com.example.japiairbnb.pdplisting;

import com.example.japiairbnb.common.RequestHandler;
import com.example.japiairbnb.common.json.CommonJsonResponse;
import com.example.japiairbnb.pdplisting.json.RequestJsonPdpListing;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class PdpListingRequestController {

    @PostMapping("/pdp_listing_booking_details")
    public ResponseEntity<CommonJsonResponse> listingPostRequest(@RequestBody RequestJsonPdpListing requestJson) {

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

    @GetMapping("/pdp_listing_booking_details")
    public ResponseEntity<CommonJsonResponse> listingGetRequest(
            @RequestParam(value = "airbnb_advertising_id", required = false) String airbnb_advertising_id,
            @RequestParam(value = "airbnb_api_key", required = false) String airbnb_api_key,
            @RequestParam(value = "airbnb_device_id", required = false) String airbnb_device_id,
            @RequestParam(value = "airbnb_oauth_token", required = false) String airbnb_oauth_token,
            @RequestParam(value = "listing_id", required = false) Integer listingId,
            @RequestParam(value = "guests", required = false) Integer guests,
            @RequestParam(value = "show_smart_promotion", required = false) Integer show_smart_promotion,
            @RequestParam(value = "number_of_adults", required = false) Integer number_of_adults,
            @RequestParam(value = "number_of_children", required = false) Integer number_of_children,
            @RequestParam(value = "number_of_infants", required = false) Integer number_of_infants) {

        RequestJsonPdpListing requestJson = new RequestJsonPdpListing();
        requestJson.setAirbnbAdvertisingId(airbnb_advertising_id != null ? airbnb_advertising_id : "");
        requestJson.setAirbnbApiKey(airbnb_api_key != null ? airbnb_api_key : "");
        requestJson.setAirbnbDeviceId(airbnb_device_id != null ? airbnb_device_id : "");
        requestJson.setAirbnbOauthToken(airbnb_oauth_token != null ? airbnb_oauth_token : "");
        requestJson.setListingId(listingId != null ? listingId : 0);
        requestJson.setGuests(guests != null ? guests : -1);
        requestJson.setShowSmartPromotion(show_smart_promotion != null ? show_smart_promotion : -1);
        requestJson.setNumberOfAdults(number_of_adults != null ? number_of_adults : -1);
        requestJson.setNumberOfChildren(number_of_children != null ? number_of_children : -1);
        requestJson.setNumberOfInfants(number_of_infants != null ? number_of_infants : -1);

        CommonJsonResponse responseJson = RequestHandler.getInstance().getResponse(requestJson);

        return new ResponseEntity<>(responseJson, HttpStatus.valueOf(responseJson.getResponseCode()));
    }

}
