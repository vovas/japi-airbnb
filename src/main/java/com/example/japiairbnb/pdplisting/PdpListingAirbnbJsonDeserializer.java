package com.example.japiairbnb.pdplisting;

import com.example.japiairbnb.common.AirbnbJsonDeserializer;
import com.example.japiairbnb.pdplisting.json.ResponseJsonPdpListing;
import com.example.japiairbnb.pdplisting.json.ResponseJsonPdpListingMain;
import com.example.japiairbnb.pdplisting.json.ResponseJsonPdpListingRate;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PdpListingAirbnbJsonDeserializer extends AirbnbJsonDeserializer {

    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public ResponseJsonPdpListing createAndFillResponse(String content) {
        ResponseJsonPdpListing responseJson = new ResponseJsonPdpListing();

        List<ResponseJsonPdpListingMain> pdpListingMains = new ArrayList<>();

        ObjectMapper m = new ObjectMapper();
        try {
            JsonNode rootNode = m.readTree(content);

            JsonNode pdpListingNode = rootNode.path("pdp_listing_booking_details");
            if (pdpListingNode.isArray()) {
                for (JsonNode pdpListingNodeItem : pdpListingNode) {

                    ResponseJsonPdpListingMain responseJsonMain = new ResponseJsonPdpListingMain();
                    ResponseJsonPdpListingRate rateJson = new ResponseJsonPdpListingRate();

                    responseJsonMain.setAvailable(pdpListingNodeItem.path("available").asBoolean());
                    responseJsonMain.setCanInstantBook(pdpListingNodeItem.path("can_instant_book").asBoolean());
                    responseJsonMain.setCancellationGuestFeePolicyLabel(
                            pdpListingNodeItem.path("cancellation_guest_fee_policy_label").asText());
                    responseJsonMain.setGuests(pdpListingNodeItem.path("guests").asInt());

                    rateJson.setAmount(pdpListingNodeItem.path("rate").path("amount").asInt());
                    rateJson.setAmountMicros(pdpListingNodeItem.path("rate").path("amount_micros").asInt(0));
                    rateJson.setAmountFormatted(pdpListingNodeItem.path("rate").path("amount_formatted").asText());
                    rateJson.setMicrosAccuracy(pdpListingNodeItem.path("rate").path("is_micros_accuracy").asBoolean());
                    rateJson.setCurrency(pdpListingNodeItem.path("rate").path("currency").asText());
                    responseJsonMain.setRate(rateJson);

                    responseJsonMain.setRateType(pdpListingNodeItem.path("rate_type").asText());
                    responseJsonMain.setServiceFeeUsd(pdpListingNodeItem.path("service_fee_usd").asDouble());
                    responseJsonMain.setTaxAmountUsd(pdpListingNodeItem.path("tax_amount_usd").asDouble());

                    pdpListingMains.add(responseJsonMain);
                }
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization failed!", e);
        }
        responseJson.setPdpListingBookingDetails(pdpListingMains);
        return responseJson;
    }
}
