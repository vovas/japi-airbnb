package com.example.japiairbnb.pdplisting;

import com.example.japiairbnb.common.ConnectionHandler;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.pdplisting.json.RequestJsonPdpListing;

public class PdpListingConnectionHandler extends ConnectionHandler {

    private static final String URL_PART1 = "https://api.airbnb.com/api/v2/pdp_listing_booking_details";


    @Override
    protected String getUrlName(CommonJsonRequest listingRequest) {
        RequestJsonPdpListing request = (RequestJsonPdpListing) listingRequest;

        String listingId = Integer.toString(request.getListingId());
        String guests = request.getGuests() == -1 ? "" : "&guests=" + Integer.toString(request.getGuests());
        String showSmartPromotion = request.getShowSmartPromotion() == -1 ? "" :
                "&show_smart_promotion=" + Integer.toString(request.getShowSmartPromotion());
        String numberOfAdults = request.getNumberOfAdults() == -1 ? "" :
                "&number_of_adults=" + Integer.toString(request.getNumberOfAdults());
        String numberOfChildren = request.getNumberOfChildren() == -1 ? "" :
                "&number_of_children=" + Integer.toString(request.getNumberOfChildren());
        String numberOfInfants = request.getNumberOfInfants() == -1 ? "" :
                "&number_of_infants=" + Integer.toString(request.getNumberOfInfants());

        return URL_PART1 + "?listing_id=" + listingId + guests + showSmartPromotion
                + numberOfAdults + numberOfChildren + numberOfInfants
                + "&_format=for_web_dateless";
    }
}
