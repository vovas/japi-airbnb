package com.example.japiairbnb.pdplisting;

import com.example.japiairbnb.common.RequestValidator;
import com.example.japiairbnb.common.json.CommonJsonRequest;
import com.example.japiairbnb.pdplisting.json.RequestJsonPdpListing;

public class PdpListingRequestValidator extends RequestValidator {

    @Override
    public boolean isRequestValid(CommonJsonRequest listingRequest) {
        RequestJsonPdpListing request = (RequestJsonPdpListing) listingRequest;
        return !(listingRequest.getAirbnbOauthToken() == null) && request.getListingId() != 0;
    }

    @Override
    public String getMessage() {
        return "Airbnb Oauth token can not be null and/or listing_id cannot be 0";
    }
}
