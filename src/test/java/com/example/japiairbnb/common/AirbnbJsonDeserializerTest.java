package com.example.japiairbnb.common;

import com.example.japiairbnb.searchlistings.json.ResponseJsonSearchListings;
import com.example.japiairbnb.searchlistings.SearchListingsAirbnbJsonDeserializer;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AirbnbJsonDeserializerTest {

    private String inputJson;
    private SearchListingsAirbnbJsonDeserializer deserializer;

    @Before
    public void setup() {
        ResponseJsonSearchListings outputResponse = new ResponseJsonSearchListings();
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/test/resources/outputFromAirbnbRaw.txt"));


            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        inputJson = sb.toString();
        deserializer = new SearchListingsAirbnbJsonDeserializer();
    }

    @Test
    public void testSetFields() {

        ResponseJsonSearchListings result = deserializer.createAndFillResponse(inputJson);

    }




}
